"""
* Create a folder exercises in you Gitlab Account
* Copy this file into the exercises folder and finish exercise
* Run pytest for the file. If all tests pass exercise is finished
* Add file or changes to the local git repository 'git add exercise01.py'
* Commit your changes 'git commit'
* Upload new commit to remote repository 'git push'

"""


def evaluate_grades(grade: int) -> None:
    """Evaluates grades from 1 to 5."""
    returnStr = ""
    if grade == 1 :
        returnStr = "Sehr gut"
    elif grade == 2 or grade == 3 :
        returnStr = "Toll"
    elif grade > 3 and grade < 6 :
        returnStr = "Das geht doch besser"
    else :
        returnStr = "Diese Note gibt es nicht"

    if grade >= 1 and grade <= 5 :
        if grade == 5 :
            returnStr = returnStr + " - Negativ!"
        else :
            returnStr.append(" - Positiv!")



# Do not change anything after this line
import pytest


@pytest.mark.parametrize(
    "grades, exp_output",
    [
        (1, "Sehr gut - Positiv!"),
        (2, "Toll - Positiv!"),
        (3, "Toll - Positiv!"),
        (4, "Das geht doch besser - Positiv!"),
        (5, "Das geht doch besser - Negativ!"),
        (6, "Diese Note gibt es nicht"),
        (53, "Diese Note gibt es nicht"),
        (-1, "Diese Note gibt es nicht"),
        (-100, "Diese Note gibt es nicht"),
    ],
)
def test_evaluate_grades(grades, exp_output):
    assert evaluate_grades(grades) == exp_output