"""
* Create a folder exercises in you Gitlab Account
* Copy this file into the exercises folder and finish exercise
* Run pytest for the file. If all tests pass exercise is finished
* Add file or changes to the local git repository 'git add exercise03.py'
* Commit your changes 'git commit'
* Upload new commit to remote repository 'git push'

Guess a Number Game

Description:
Write a script that asks the user to guess a number between 1 and 10. The script randomly set a number between 1 and 10.
If the user guesses the correct number the script writes "You got it" to the terminal. If the value is to small the
script writes "Too small you have X tries left". If the value is to high the scripts writes "Too high you have X tries
left". If the user guess is not in the range between 1 and 10 the user should repeat the input with "Number is not in
range. Guess a number between 1 and 10". This does not count a try. The user have 3 tries to guess the correct number.
If the user fails print "You lost the game!"

* First think about which parts are needed
* Tranlates this parts to functions
* Compile all parts together in a main function

Let's do it step by step.

* main loop (play again, exit) while
* Set random number
* User input
* Define loop
* Compare user input with random number
* User output
* Win, loose

"""
import random


def get_random_number(min_val: int, max_val: int) -> int:
    """
    Function should return a random number between 'min_val' and 'max_val' including both min_val' and 'max_val'.

    Use function randint from the random module - https://docs.python.org/3/library/random.html

    """
    return random.randint(min_val, max_val)


def process_user_input(min_val: int, max_val: int) -> int:
    """
    Convert user input string to integer.

    Use input() for the user request.
    Repeat as long if value is in range.

    """
    print("Take a guess")
    guessed_number = int(input())
    while guessed_number < min_val or guessed_number > max_val:
        print(f"Number is not in range. Guess a number between {min_val} and {max_val}")
        guessed_number = int(input())
    return guessed_number


def iterate_guesses(retries: int = 3) -> bool:
    """
    Loop over all retries.

    Return True if user guess the correct number. Return False if user can not guess number in 'retries' rounds.

    """
    tries = 0
    target_number = get_random_number()
    while tries <= retries:
        guessed_number = process_user_input(1, 10)
        if guessed_number == target_number:
            return True
        elif guessed_number < target_number:
            print(f"Your guess was too low. You have {retries-tries} tries left.")
        else:
            print(f"Your guess was too high. You have {retries-tries} tries left.")

    return False


def guess_a_number() -> None:
    """
    Put all together in this function.

    """
    if iterate_guesses():
        print("You got it!")
    else:
        print("You lost the game!")


# Call the game
if __name__ == "__main__":
    guess_a_number()


# Do not change anything after this line
import pytest


@pytest.mark.parametrize(
    "min_val, max_val, exp_output",
    [(1, 10, 5)],
)
def test_get_random_number(min_val, max_val, exp_output, mocker):
    mocker.patch("random.randint", return_value=5)
    assert get_random_number(min_val, max_val) == exp_output


def test_process_user_input(mocker):
    mocker.patch("exercise02.input", return_value=5)
    assert process_user_input(3, 6) == 5


def test_iterate_guesses_loose(mocker):
    mocker.patch("exercise02.get_random_number", return_value=1)
    mock_user_input = mocker.patch(
        "exercise02.process_user_input", side_effect=[5, 4, 3, 2, 6]
    )
    result = iterate_guesses(5)

    assert mock_user_input.call_count == 5
    assert result is False


def test_iterate_guesses_win(mocker):
    mocker.patch("exercise02.get_random_number", return_value=1)
    mock_user_input = mocker.patch(
        "exercise02.process_user_input", side_effect=[5, 4, 1]
    )
    result = iterate_guesses(3)

    assert mock_user_input.call_count == 3
    assert result is True
