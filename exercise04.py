import io

def word_count(file_in: str, file_out: str) -> str:
    """
    Schreiben Sie ein Programm, dass ein Textfile 'file_in' öffnet, den Text einliest und folgende Informationen in ein
    weiteres File 'file_out' schreibt: Filename, Anzahl der Zeilen, Anzahl der Wörter und Anzahl aller Zeichen.

    test.txt
    Lorem ipsum dolor sit\n
    amet, consectetur\n
    adipiscing elit.

    result = word_count('test.txt', 'outfile')

    print(result)
    test.txt 3 8 56

    Notes:
        Erzeut outfile mit selben Inhalt wie return Wert als Nebeneffekt.

    outfile
    test.txt 3 8 56

    Args:
        file_in:
            Filename vom Input File
        file_out:
            Filename vom Output File

    Returns:
        str

    """
    # open files with context manager to analyze and write to
    with open(file_in, "r") as f_in:
        with open(file_out, "w+") as f_out:
            # Write filename to stream
            f_out.write(file_in + " ")
            # get line count and write to stream
            line_count = 0
            for line in f_in:
                line_count += 1
            f_out.write(str(line_count) + " ")
            # get word count and write to stream
            data = f_in.read()
            words = data.split()
            f_out.write(str(len(words)) + " ")
            # get char count and write to stream
            f_out.write(str(len(data)) + " ")

    print(f_out)
    return f_out

word_count("file.txt", "out.txt")

def read_fasta(file_in: io.IOBase, file_out: io.IOBase) -> str:
    """
    Liest eine DNA Fasta Sequenz von einem Stream ein und übersetzt die DNA Sequenz in eine Protein Sequenz und schreibt
    sie in den Output Stream und gibt sie auch als Rückgabewert zurück. Achten sie darauf das Stoppcodone (*) nicht mit
    ausgegeben werden.

    Notes:
        Schreibt result auch in den Out-Stream.
        Um das parsen zu erleichtern ist nur eine Sequenz im Fasta File. Siehe file CCDS22034.1.fa

    CCDS22034.1.fa
    >CCDS22034.1
    ATGGCCCTG
    TGGATGCGC
    TTCCTGCCC

    with open(CCDS22034.1.fa, "r") as f_in:
        with open(file_out, "w") as f_out:
            result = read_fasta(f_in, f_out)
    print(result)

    MALWMRFLP

    Args:
        file_in:
            Stream Objekt z.B: open("File", "r")
        file_out:
            Stream Objekt z.B: open("File", "w")

    Returns:
        str

    """

    bases = "tcag"
    codons = [a + b + c for a in bases for b in bases for c in bases]
    amino_acids = 'FFLLSSSSYY**CC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG'
    codon_table = dict(zip(codons, amino_acids))

    with open(file_in, "r") as f_in:
        with open(file_out, "w+") as f_out:
            line_list = f_in.readlines()[1:]
            seq = ''
            for line in line_list:
                seq += line
            seq = seq.lower().replace('\n', '')
            print(seq)
            protein = ''
            for i in range(0, len(seq), 3):
                codon = seq[i: i + 3]
                amino_acid = codon_table.get(codon, '*')
                if amino_acid != '*':
                    protein += amino_acid
                else:
                    break
            f_out.write(protein)

read_fasta("CCDS22034.1.fa", "outprotein.txt")
# Freiwillige extra Aufgabe
# Schreiben sie eine read_fastas Funktion, die mehrere DNA Sequenzen aus dem Fasta File (multi_fasta.fa) lesen kann und
# eine Liste der Proteinsequenzen zurückgibt.